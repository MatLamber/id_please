﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BrazoAnim : MonoBehaviour
{
    private Animator controlador;
    

    void Start()
    {
        controlador = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (GameManager.Instancia.MostrarCarpeta == 1)
        {
            controlador.Play("Mostrar ");
        }
        else
        {
            controlador.Play("Ocultar");
        }
    }
    
    private void ResetearEleccion()
    {
        GameManager.Instancia.Eleccion = 0;
    }

}

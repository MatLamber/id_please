﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager Instancia { get; private set; }
    //Campos serializados
    [SerializeField] private GameObject[] personajes;
    [SerializeField] private GameObject brazoCarpeta;
    [SerializeField] private GameObject alfombra;
    [SerializeField] private GameObject fondo;
    [SerializeField] private GameObject panelMenu;
    [SerializeField] private GameObject panelPlay;
    [SerializeField] private GameObject panelGameOver;
    [SerializeField] private GameObject swipe;
    
    private List <GameObject> listaPersonajes = new List<GameObject>();
    private GameObject personajeInstancia;
    private GameObject alfombraInstancia;
    private GameObject fondoInstancia;
    private GameObject brazoCarpetaInstancia;
    private GameObject swipeInstancia;
    private Animator controlador;
    private bool pasoAlFrente = false;
    private int jugadorActual;
    private int numeroDePersonajes;
    private int iterador = 0;
    private int offset = 0;
    
    public enum EstadoDeJuego {MENU, INIT ,PLAY, NIVEL_COMPLETADO, CARGANDO_NIVEL, GAMEOVER}
    //Campo mediante el cual se acceden a los diferentes estados de juego
    private EstadoDeJuego estadoActual;
    //Bandera para controlar si esa efectuando un cambio de estado
    private bool cambioDeEstado;

    private int mostrarCarpeta;
    private int eleccion;
    
    public int MostrarCarpeta
    {
        get { return mostrarCarpeta;}
        set {mostrarCarpeta = value;}
    }

    public int Eleccion
    {
        get { return eleccion;}
        set {eleccion = value;}
    }

    void Start()
    {
        Instancia = this;
        CambiarEstado(EstadoDeJuego.MENU);
    }
    public void CambiarEstado(EstadoDeJuego nuevoEstado, float retraso = 0)
    {
        StartCoroutine(RetrasoCambio(nuevoEstado, retraso));
    }
    IEnumerator RetrasoCambio (EstadoDeJuego nuevoEstado, float retraso = 0)
    {
        cambioDeEstado = true;
        yield return new WaitForSeconds(retraso);
        TerminarEstado();
        estadoActual = nuevoEstado;
        ComenzarEstado(nuevoEstado);
        cambioDeEstado = false;
    }

    void ComenzarEstado(EstadoDeJuego nuevoEstado)
    {
        switch (nuevoEstado)
        {
           case EstadoDeJuego.MENU :
                panelMenu.SetActive(true);
               break;
           case EstadoDeJuego.INIT :
               MostrarCarpeta = 0;
               eleccion = 0;
               alfombraInstancia = Instantiate(alfombra);
               fondoInstancia = Instantiate(fondo);
               brazoCarpetaInstancia = Instantiate(brazoCarpeta);
               CambiarEstado(EstadoDeJuego.CARGANDO_NIVEL);
               break;
           case EstadoDeJuego.PLAY :
               controlador = listaPersonajes[jugadorActual].GetComponent<Animator>();
               controlador.SetInteger("Caminar", 1);
               Invoke("HacerEsperar", 1f);
               pasoAlFrente = true;
               Invoke("InstanciarSwipe", 1f);
               panelPlay.SetActive(true);
               break;
           case EstadoDeJuego.NIVEL_COMPLETADO :
               break;
           case EstadoDeJuego.CARGANDO_NIVEL :
               numeroDePersonajes = Random.Range(2, 6);
               while (iterador < numeroDePersonajes)
               {
                   personajeInstancia = Instantiate(personajes[iterador],new Vector3(personajes[iterador].transform.position.x,personajes[iterador].transform.position.y, (personajes[iterador].transform.position.z + offset)), Quaternion.AngleAxis( 180f,Vector3.up));
                   listaPersonajes.Add(personajeInstancia);
                   iterador++;
                   offset += 1;
               }
               jugadorActual = 0;
               CambiarEstado(EstadoDeJuego.PLAY);
               break;
           case  EstadoDeJuego.GAMEOVER :
               Destroy(alfombraInstancia);
               Destroy(fondoInstancia);
               Destroy(brazoCarpetaInstancia);
               panelGameOver.SetActive(true);
               break;
        }
    }
    void Update()
    {
        switch (estadoActual)
        {
            case EstadoDeJuego.MENU :
                break;
            case EstadoDeJuego.INIT :
                break;
            case EstadoDeJuego.PLAY :
               
                if (jugadorActual < listaPersonajes.Count)
                {
                    controlador = listaPersonajes[jugadorActual].GetComponent<Animator>();
                    if (Eleccion == 1)
                    {
                        controlador.SetInteger("Aceptado", 1);
                        Invoke("DestruiPersonaje", 1f);
                        jugadorActual++;
                        if (jugadorActual < listaPersonajes.Count)
                        {
                            
                            controlador = listaPersonajes[jugadorActual].GetComponent<Animator>();
                            controlador.SetInteger("Caminar", 1);
                            Invoke("HacerEsperar", 1f);
                        }
                        Eleccion = 0;
                    }

                    if (Eleccion == -1)
                    {
                        controlador.SetInteger("Rechazado", 1);
                        Invoke("DestruiPersonaje", 1f);
                        jugadorActual++;
                        if (jugadorActual < listaPersonajes.Count)
                        {
                           
                            controlador = listaPersonajes[jugadorActual].GetComponent<Animator>();
                            controlador.SetInteger("Caminar", 1);
                            Invoke("HacerEsperar", 1f);
                        }
                        Eleccion = 0;
                    }
                }
                else
                {
                    CambiarEstado(EstadoDeJuego.GAMEOVER,2f);
                }

                break;
            case EstadoDeJuego.NIVEL_COMPLETADO :
                break;
            case EstadoDeJuego.CARGANDO_NIVEL :
                break;
            case  EstadoDeJuego.GAMEOVER :
                break;
        }
    }

    void TerminarEstado()
    {
        switch (estadoActual)
        {
            case EstadoDeJuego.MENU :
                panelMenu.SetActive(false);
                break;
            case EstadoDeJuego.INIT :
                break;
            case EstadoDeJuego.PLAY :
                panelPlay.SetActive(false);
                break;
            case EstadoDeJuego.NIVEL_COMPLETADO :
                break;
            case EstadoDeJuego.CARGANDO_NIVEL :
                break;
            case  EstadoDeJuego.GAMEOVER :
                panelGameOver.SetActive(false);
                break;
        }
    }

    public void BotonJugar()
    {
        CambiarEstado(EstadoDeJuego.INIT);
    }
    public void BotonMostrarCarpeta()
    {
        if (MostrarCarpeta == 0)
        {
            MostrarCarpeta = 1;
        }
        else
        {
            MostrarCarpeta = 0;
        }
    }

    void HacerEsperar()
    {
        controlador.SetInteger("Caminar", -1);
    }
    void InstanciarSwipe()
    {
        Instantiate(swipe);
    }

    void DestruiPersonaje()
    {
        Destroy(listaPersonajes[jugadorActual-1]);
    }
}

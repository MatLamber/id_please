﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Swipe : MonoBehaviour
{
    private Vector2 posicionInicial ;
    void Update () 
    {
        if( Input.GetMouseButtonDown(0) )
        {
            posicionInicial = Input.mousePosition;
        }
        if( Input.GetMouseButtonUp(0))
        {       
            Calcular(Input.mousePosition);
        }
    }
    void Calcular(Vector3 posicionFinal)
    {
        float disX = Mathf.Abs(posicionInicial.x - posicionFinal.x);
        float disY = Mathf.Abs(posicionInicial.y - posicionFinal.y);
        if(disX>0 || disY>0)
        {
            if (disX > disY) 
            {
                if (posicionInicial.x > posicionFinal.x)
                {
                    Debug.Log("izquierda");
                    GameManager.Instancia.Eleccion = -1;
                }
                else
                {
                    Debug.Log("derecha");
                    GameManager.Instancia.Eleccion = 1;
                }
            }
            else 
            {   
                if (posicionInicial.y > posicionFinal.y )
                {
                    Debug.Log("abajo");
                }
                else
                {
                    Debug.Log("arriba");
                }
            }
        }
    }
}
